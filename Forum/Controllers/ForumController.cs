﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Forum.BaseLogic.DataHandler.Enums;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Web.Models;
using Forum.Repository.DataBase.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Forum.Web.Controllers
{
    public class ForumController: Controller
    {
      
        private readonly IDataHandlerService _dataHandlerService;


        public ForumController(IDataHandlerService dataHandlerService)
        {
            _dataHandlerService = dataHandlerService;  
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AllMessages(int? page, SortParam sortParam)
        {
            ViewData["NameSort"] = sortParam == SortParam.UserNameAsc ? SortParam.UserNameDesc : SortParam.UserNameAsc;
            ViewData["DateSort"] = sortParam == SortParam.DateAsc ? SortParam.DateDesc : SortParam.DateAsc;
            ViewData["CurrentSort"] = sortParam;

            var pageNumber = page ?? 1;

            return pageNumber < 0 
                ? PartialView("WrongPageNumber") 
                : PartialView ( new MessageListViewModel(_dataHandlerService.GetPaganedMessageList(pageNumber, sortParam)));
        }

        [HttpGet]
        public IActionResult Message(int? messageId)
        {

            ViewData["Message"] = "Write your message";
            ViewBag.MessageAction = MessageAction.Add;
            return PartialView();
        }

        [HttpGet]
        public IActionResult UpdateMessage(int id)
        {
            var messageView = Mapper.Map<MessageViewModel>(_dataHandlerService.GetDataHandler().GetMessageById(id));
            ViewBag.MessageAction = MessageAction.Update;
            return PartialView("Message", messageView);
        }

        [HttpPost]
        public IActionResult DeleteMessage(int id, int page, SortParam sortParam)
        {
            return PartialView("AllMessages",new MessageListViewModel(_dataHandlerService.DeleteMessageAndGetNewList(id,page,sortParam)));
        }

        [HttpPost]
        public IActionResult Message(MessageViewModel messageView, MessageAction messageAction)
        {
            if (!ModelState.IsValid) return PartialView(messageView);
            var messageModel = Mapper.Map<MessageModel>(messageView);
            _dataHandlerService.AddOrUpdateMessage(messageModel,messageAction);
            return PartialView("success");
        }

       
    }
}