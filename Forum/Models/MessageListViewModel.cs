﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.BaseLogic.DataHandler;
using Forum.Repository.DataBase;
using Forum.Repository.DataBase.Models;
using PagedList.Core;
using AutoMapper;
namespace Forum.Web.Models
{
    public class MessageListViewModel
    {
        public PaginatedMessageList<MessageViewModel> PaginatedMessageList { get; set; }

        public IQueryable<MessageViewModel> GetMesageList()
        {
            return PaginatedMessageList.MessageList;
        }

        public int GetPageNumber()
        {
            return PaginatedMessageList.PageNumber;
        }

        public bool GetHasNextPage()
        {
            return PaginatedMessageList.HasNextPage;
        }

        public bool GetHasPrevPage()
        {
            return PaginatedMessageList.HasPreviousPage;
        }

        public MessageListViewModel(PaginatedMessageList<MessageModel> paginatedMessageList)
        { 
            var messageList = new List<MessageViewModel>();
            foreach (var message in paginatedMessageList.MessageList)
            {
                messageList.Add(Mapper.Map<MessageViewModel>(message));
            }
            PaginatedMessageList =new PaginatedMessageList<MessageViewModel>(paginatedMessageList.PageNumber,paginatedMessageList.PageSize,paginatedMessageList.TotalPages, messageList.AsQueryable());
        }
    }
}
