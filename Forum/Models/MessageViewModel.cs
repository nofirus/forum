﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Repository.DataBase.Models;

namespace Forum.Web.Models
{
    public class MessageViewModel
    {
        
        public int Id { get; set; }
    
        [Required]
        [RegularExpression("[a-zA-Z0-9]{0,}", ErrorMessage = "Wrong name, only Latin error")]
        [StringLength(maximumLength:50)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "mm/dd/yyyy")]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(maximumLength:50)]
        public string MessageTopic { get; set; }

        [Required]
        [StringLength(maximumLength:50)]
        public string MessageСategory { get; set; }

        [Required]
        [StringLength(maximumLength: 256)]
        public string MessageText { get; set; }
    }
}
