﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Forum.Repository.DataBase.DataBaseContext;
using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Repository.DataBase.MessageRepository;
using Forum.Repository.DataBase.Models;
using Forum.Repository.DataBase.Services;
using Forum.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
           
            services.AddTransient<IMessageRepositoryService, MessageRepositoryService>();
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IDataHandlerService,DataHandlerService>();
            services.AddTransient<IDataHandler, DataHandler>();

    
            services.AddDbContext<MessageContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<MessageViewModel, MessageModel>(); //.ForMember(m => m.Id, i => i.Ignore());
            });




        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();         
            }
           

            app.UseStaticFiles();
            app.UseStatusCodePages();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "Forum",
                    template: "{controller=Forum}/{action=Index}/{id?}");


            });
        }
    }
}
