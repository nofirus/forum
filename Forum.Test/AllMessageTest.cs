﻿using System.Collections.Generic;
using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Enums;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Repository.DataBase.Models;
using Forum.Repository.DataBase.Services;
using Forum.Web.Controllers;
using Forum.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Forum.Test
{
   public class AllMessageTest
    {

        public static List<object[]> TestPages = new List<object[]>()
        {
            new object[] {1, SortParam.UserNameAsc,true,false},
            new object[] {1, SortParam.UserNameDesc,true,false},
            new object[] {1, SortParam.DateAsc,true,false},
            new object[] {1, SortParam.DateDesc,true,false},
            new object[] {10, SortParam.DateDesc,false,true},
            new object[] {-1, null,true,true},
            new object[] {1, -100,true,false},
            new object[] {10, null,true,true},
            new object[] {0,-1,true,true},
            new object[] {null,-1,true,true},
            new object[] {null,null,true,true},
            new object[] {-1,-1,true,true},
            new object[] {12, SortParam.DateAsc,true,true},

        };
        [Theory]
        [MemberData(nameof(TestPages))]
        public void AllMessageHasNextHasPrevPageTest(int pageNumber, SortParam sortParam, bool hasNextPage, bool hasPrevPage)
        {
            var messagerepositoryService = new Mock<IMessageRepositoryService>();
            var messageList = new List<MessageModel>()
            {
                new MessageModel(),
                new MessageModel(),
                new MessageModel(),
                new MessageModel(),
                new MessageModel(),
                new MessageModel()
            };
            messagerepositoryService.Setup(x => x.GetMessageRepository().GetMessageList()).Returns( messageList);
            var controller = new ForumController(new DataHandlerService(new DataHandler(messagerepositoryService.Object)));
            var result = Assert.IsType<PartialViewResult>(controller.AllMessages(pageNumber, sortParam));
            var model = Assert.IsType<MessageListViewModel>(result.Model);
            Assert.Equal(hasNextPage, model.PaginatedMessageList.HasNextPage);
            Assert.Equal(hasPrevPage, model.PaginatedMessageList.HasPreviousPage);
        }    
    }
}
