﻿using System;
using System.Collections.Generic;
using System.Text;
using Forum.Web.Controllers;
using Forum.Web.Models;
using Moq;
using Xunit;
using Forum;
using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Repository.DataBase.MessageRepository;
using Forum.Repository.DataBase.Models;

namespace Forum.Test
{
   public  class BaseLogic
    {
        public static List<object[]> TestMessagesList = new List<object[]>()
        {
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic =  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Parse("10/10/2010"),
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "afffsdf",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "___________________________"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "",
                    MessageСategory = "asd",
                    UserName = ""
                }
            }
        };

        [Theory]
        [MemberData(nameof(TestMessagesList))]
        public void AddMessageTest(MessageModel messageModel)
        {
            var dataHandleServiceMock = new Mock<IDataHandlerService>();
            dataHandleServiceMock.Setup(x => x.GetDataHandler()).Returns(new DataHandler(new MessageRepositoryService(new Mock<IMessageRepository>().Object)));
            var dataHandleService = dataHandleServiceMock.Object;
            var dataHandler=dataHandleService.GetDataHandler();
            dataHandler.AddNewMessage(messageModel);  
        }
    }
}
