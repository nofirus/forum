﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Forum.Web.Models;
using Xunit;

namespace Forum.Test
{
   public class MessageTest
    {
        public static List<object[]> TestMessagesList = new List<object[]>()
        {
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic =  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Parse("10/10/2010"),
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "afffsdf",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "___________________________"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "asd",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                    MessageСategory = "asd",
                    UserName = "Me"
                }
            },
            new object[]
            {
                new MessageViewModel()
                {
                    Date = DateTime.Now,
                    MessageText = "asd",
                    MessageTopic = "",
                    MessageСategory = "asd",
                    UserName = ""
                }
            }
        };


        [Theory]
        [MemberData(nameof(TestMessagesList))]
        public void MessageValidationPostTest(MessageViewModel messageViewModel)
        {
            Assert.True(ValidateModel(messageViewModel));
        }

        private bool ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            return Validator.TryValidateObject(model, ctx, validationResults, true);
        }
    }
}
