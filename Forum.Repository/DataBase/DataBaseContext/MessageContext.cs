﻿using Forum.Repository.DataBase.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository.DataBase.DataBaseContext
{
    public sealed class MessageContext: DbContext
    {
        public DbSet<MessageModel> Messages { get; set; }

        public MessageContext(DbContextOptions options)
            : base(options)
        {
            
        }
    }

}

