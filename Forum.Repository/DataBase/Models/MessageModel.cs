﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Forum.Repository.DataBase.Models
{
    public class MessageModel:IComparable<MessageModel>
    {
        [Key]
        public int Id { get; set; }
        [StringLength(maximumLength:50)]
        public string UserName { get; set; }
      
        public DateTime Date { get; set; }
   
        public string MessageTopic { get; set; }
        [StringLength(maximumLength:50)]
        public string MessageСategory { get; set; }
       
        [StringLength(maximumLength:250)]
        public string MessageText { get; set; }

        public int CompareTo(MessageModel other)
        {
            if (Date.Equals(other.Date))
            {
                return Id.CompareTo(other.Id);
            }
            return Date.CompareTo(other.Date);
        }

        public override string ToString()
        {
            return "ID: " + Id + " UserName: " + UserName + " Date " + Date + " MessageTopic: " + MessageTopic +
                   "MessageCategory" + MessageСategory + "MessageText: " + MessageText;
        }
    }
}
