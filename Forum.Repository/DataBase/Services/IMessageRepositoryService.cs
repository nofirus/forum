﻿using Forum.Repository.DataBase.MessageRepository;
using Forum.Repository.DataBase.Models;
using System.Collections.Generic;

namespace Forum.Repository.DataBase.Services
{
   public interface IMessageRepositoryService
   {
        IMessageRepository MessageRepository { get; }
        List<MessageModel> GetEldestMessagesByUserName(string userName, int amount);

    }
}
