﻿using System.Collections.Generic;
using Forum.Repository.DataBase.Services;
using System.Linq;
using Forum.Repository.DataBase.Models;

namespace Forum.Repository.DataBase.MessageRepository
{
    public class MessageRepositoryService:IMessageRepositoryService

    {
        public IMessageRepository MessageRepository { get; }

        public MessageRepositoryService(IMessageRepository messageRepository)
        {
            MessageRepository = messageRepository;
        }

        public List<MessageModel> GetEldestMessagesByUserName(string userName, int amount)
        {
            List<MessageModel> messageList = MessageRepository.GetAllMessagesOfUser(userName);
            messageList.Sort();
            return messageList.Take(amount).ToList();
        }
    }
}
