﻿using System.Collections.Generic;
using System.Linq;
using Forum.Repository.DataBase.DataBaseContext;
using Forum.Repository.DataBase.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository.DataBase.MessageRepository
{
    public class MessageRepository : IMessageRepository
    {

        private readonly MessageContext _messageContext;

        public MessageRepository(MessageContext messageContext)
        {
            this._messageContext = messageContext;
        }

        public void AddMessage(MessageModel message)
        {
            _messageContext.Messages.Add(message);
            _messageContext.SaveChanges();
        }

        public int DeleteMessage(int id)
        {
            var message = (MessageModel)_messageContext.Messages.Select(x => x).First(x => x.Id.Equals(id));
            _messageContext.Messages.Remove(message);
            return _messageContext.SaveChanges();
        }

        public List<MessageModel> GetMessageList()
        {
            return _messageContext.Messages.ToList();
        }

        public MessageModel GetMessageById(int messageId)
        {
            return _messageContext.Messages.Find(messageId);
        }

        public void UpdateMessage(MessageModel message)
        {
            _messageContext.Messages.Update(message);
            _messageContext.SaveChanges();
        }

        public DbContext GetMessageContext()
        {
            return _messageContext;
        }

        public List<MessageModel> GetAllMessagesOfUser(string userName)
        {
            return _messageContext.Messages.Where(x => x.UserName.Equals(userName)).ToList();
        }

        public List<string> AllUsersName()
        {
            HashSet<string> set = _messageContext.Messages.Select(x => x.UserName).ToHashSet();
            return set.ToList();
        }
    }
}
