﻿using System.Collections.Generic;
using Forum.Repository.DataBase.Models;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repository.DataBase.MessageRepository
{
    public interface IMessageRepository
    {
        void AddMessage(MessageModel message);
        int DeleteMessage(int id);
        List<MessageModel> GetMessageList();
        MessageModel GetMessageById(int messageId);
        void UpdateMessage(MessageModel message);
        DbContext GetMessageContext();
        List<string> AllUsersName();
        List<MessageModel> GetAllMessagesOfUser(string userName);
    }
}
