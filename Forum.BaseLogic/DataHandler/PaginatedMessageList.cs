﻿using System;
using System.Linq;
using Microsoft.VisualBasic.CompilerServices;
using PagedList.Core;

namespace Forum.BaseLogic.DataHandler
{
    public class PaginatedMessageList<T>
    {

        public int PageNumber { get; private set; }
        public int PageSize { get; set; }
        public int TotalPages { get; private set; }
        public IQueryable<T> MessageList { get; set; }

        public PaginatedMessageList(int pageNumber, int pageSize, int totalPages, IQueryable<T> messageList)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalPages = totalPages;
            MessageList = messageList;
        }

        public PaginatedMessageList(IQueryable<T> items, int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            MessageList = items;
            PageSize = pageSize;
        }

        public bool HasPreviousPage => (PageNumber > 1);

        public bool HasNextPage => (PageNumber < TotalPages);

        public IPagedList<T> GetPaginatedMessageList()
        {
            return  MessageList.AsQueryable().ToPagedList(PageNumber, PageSize);
        }
    }
}
