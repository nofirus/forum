﻿namespace Forum.BaseLogic.DataHandler.Enums
{
    public enum SortParam
    {
        UserNameAsc,
        UserNameDesc,
        DateAsc,
        DateDesc
    }
}
