﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Enums;
using Forum.Repository.DataBase.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace Forum.BaseLogic.DataHandler.Services
{
    public class DataHandlerService:IDataHandlerService
    {
        private IDataHandler dataHandler;
        private static int _pageSize=5;//Do no forget to ask

        public DataHandlerService(IDataHandler dataHandler)
        {
           this.dataHandler = dataHandler;
        }

        public IDataHandler GetDataHandler()
        {
            return dataHandler;
        }

        public PaginatedMessageList<MessageModel> GetPaganedMessageList(int pageNumber, SortParam sortParam)
        {
          return dataHandler.GetPaganedMessageList(pageNumber, _pageSize, sortParam);
        }

        public void DeleteMessage(int id)
        {
            dataHandler.DeleteMessage(id);
        }

        public PaginatedMessageList<MessageModel> DeleteMessageAndGetNewList(int id, int page, SortParam sortParam)
        {
           var count= dataHandler.GetPaganedMessageList(page, _pageSize, sortParam).GetPaginatedMessageList().Count();
            var newPage = page;
            if (count == 1)
            {
                newPage -= 1;
            }
            DeleteMessage(id);
            return dataHandler.GetPaganedMessageList(newPage, _pageSize, sortParam);
        }

        public void AddOrUpdateMessage(MessageModel message, MessageAction messageAction)
        {
            switch (messageAction)
            {
                case MessageAction.Add:
                {
                    dataHandler.AddNewMessage(message);
                    break;
                }
                case MessageAction.Update:
                {
                   dataHandler.UpdateMessage(message);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(messageAction), messageAction, null);
            }
        }

        public List<MessageModel> GetSuperfluousMessages()
        {
            List<MessageModel> messageList = new List<MessageModel>();
            List<string> usersList = dataHandler.GetAllUsersName();
            foreach (var userName in usersList)
            {
                int counter = 10 - dataHandler.GetAmountOfUserMessages(userName);
                if (counter < 0)
                {
                    messageList.AddRange(dataHandler.MessageRepositoryService.GetEldestMessagesByUserName(userName, Math.Abs(counter)));
                }
            }
            return messageList;
        }
    }
}
