﻿using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Enums;
using Forum.Repository.DataBase.Models;
using System.Collections.Generic;

namespace Forum.BaseLogic.DataHandler.Services
{
  public  interface IDataHandlerService
    {
        IDataHandler GetDataHandler();
        PaginatedMessageList<MessageModel> GetPaganedMessageList(int pageNumber, SortParam sortParam);
        void AddOrUpdateMessage(MessageModel message, MessageAction messageAction);
        void DeleteMessage(int id);
        PaginatedMessageList<MessageModel> DeleteMessageAndGetNewList(int id, int page, SortParam sortParam);
        List<MessageModel> GetSuperfluousMessages();
    }
}
