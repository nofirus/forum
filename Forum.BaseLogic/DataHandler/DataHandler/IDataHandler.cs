﻿using Forum.BaseLogic.DataHandler.Enums;
using Forum.Repository.DataBase.Models;
using Forum.Repository.DataBase.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Forum.BaseLogic.DataHandler.DataHandler
{
    public interface IDataHandler
    {
        void AddNewMessage(MessageModel message);
        int DeleteMessage(int id);
        PaginatedMessageList<MessageModel> GetPaganedMessageList(int pageNumber, int pageSize, SortParam sortParam);
        MessageModel GetMessageById(int messageId);
        void UpdateMessage(MessageModel message);
        int GetAmountOfUserMessages(string userName );
        MessageModel GetFirstMessageOfUser(string userName);
        DbContext GetMessaContext();
        List<string> GetAllUsersName();
        IMessageRepositoryService MessageRepositoryService { get; set; }

    }
}
