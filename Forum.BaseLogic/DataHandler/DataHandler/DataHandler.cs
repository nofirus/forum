﻿using System;
using System.Linq;
using Forum.Repository.DataBase.Models;
using System.Linq.Dynamic.Core;
using Forum.BaseLogic.DataHandler.Enums;
using Forum.Repository.DataBase.Services;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Forum.BaseLogic.DataHandler.DataHandler
{
    public class DataHandler: IDataHandler
    {

        private IMessageRepositoryService messageRepositoryService;

        public IMessageRepositoryService MessageRepositoryService { get => messageRepositoryService; set => messageRepositoryService = value; }

        public DataHandler(IMessageRepositoryService messageRepositoryService)
        {
            MessageRepositoryService =messageRepositoryService;
        }

        public void AddNewMessage(MessageModel message)
        {
            MessageRepositoryService.MessageRepository.AddMessage(message);
        }

        public int DeleteMessage(int id)
        {
            return MessageRepositoryService.MessageRepository.DeleteMessage(id);
        }

        public PaginatedMessageList<MessageModel> GetPaganedMessageList(int pageNumber, int pageSize, SortParam sortParam)
        {
            var messages =MessageRepositoryService.MessageRepository.GetMessageList().AsQueryable();
            return new PaginatedMessageList<MessageModel>(SortMessages(messages,sortParam), messages.Count(), pageNumber, pageSize);
        }

        public MessageModel GetMessageById(int messageId)
        {
            return MessageRepositoryService.MessageRepository.GetMessageById(messageId);
        }

        public void UpdateMessage(MessageModel message)
        {
            MessageRepositoryService.MessageRepository.UpdateMessage(message);
        }

        public int GetAmountOfUserMessages(string userName)
        {
           return MessageRepositoryService.MessageRepository.GetMessageList().Count(x => x.UserName.Equals(userName));
        }

        public MessageModel GetFirstMessageOfUser(string userName)
        {
            return MessageRepositoryService.MessageRepository.GetMessageList()
                .Where(x => x.UserName.EndsWith(userName)).First();
        }

        public DbContext GetMessaContext()
        {
            return MessageRepositoryService.MessageRepository.GetMessageContext();
        }

        public IQueryable<MessageModel> SortMessages(IQueryable<MessageModel> messages,SortParam sortParam)
        {
            var sortField = sortParam.ToString();
            sortField = sortField.Insert(
                sortParam.ToString().Contains("Asc")
                    ? sortField.IndexOf("Asc", StringComparison.Ordinal)
                    : sortField.IndexOf("Desc", StringComparison.Ordinal)," ");
            return messages.AsQueryable().OrderBy(sortField);
        }

        public List<string> GetAllUsersName()
        {
            return MessageRepositoryService.MessageRepository.AllUsersName();
        }
    }
}
