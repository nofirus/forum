﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Runtime.Serialization;
using Forum.Repository.DataBase.Services;
using System.Net.Mail;
using Forum.Repository.DataBase.DataBaseContext;
using Microsoft.EntityFrameworkCore;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Repository.DataBase.Models;
using Microsoft.Extensions.Configuration;

namespace MesasgeMonitorWebJobCore
{
    public class WebJobsMethods // Other class names are available.
    {
         private IDataHandlerService dataHandlerService;
        private string UserName { get; set; }
        private string Password { get; set; }
        private string MailTo { get; set; }

         public WebJobsMethods(IDataHandlerService service)
         {
            dataHandlerService = service;
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .Build();
            UserName = configuration["UserName"];
            Password = configuration["Password"];
            MailTo = configuration["MailTo"];
        }

        public void Do([TimerTrigger("*/30 * * * * *")]TimerInfo timer, TextWriter log)
        {
            Console.WriteLine("Check messages");
           List<MessageModel> messageList=dataHandlerService.GetSuperfluousMessages();
            if (messageList.Count > 0)
            {
                foreach (var message in messageList)
                {
                     dataHandlerService.DeleteMessage(message.Id);
                    SendEmail(message.ToString());
                }
            }
        }

        private  void SendEmail(string text)
        {
            Console.WriteLine("Send");
            MailMessage message = new MailMessage();
            message.To.Add(new MailAddress(MailTo, "Pavel"));
            message.From = new MailAddress(UserName, "ssonic1");
            message.Subject = "Azure Web App Email";
            message.Body = text;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(UserName, Password),
                Port = 587,
                Host = "smtp.gmail.com",
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true
            };
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }


    }
}
