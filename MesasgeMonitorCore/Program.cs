﻿using System;
using System.Configuration;
using System.IO;
using Forum.BaseLogic.DataHandler.DataHandler;
using Forum.BaseLogic.DataHandler.Services;
using Forum.Repository.DataBase.DataBaseContext;
using Forum.Repository.DataBase.MessageRepository;
using Forum.Repository.DataBase.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
 

namespace MesasgeMonitorWebJobCore
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var configuration = new JobHostConfiguration();
            configuration.Queues.MaxPollingInterval = TimeSpan.FromSeconds(10);
            configuration.Queues.BatchSize = 1;
            configuration.JobActivator = new MessageJobActivator(serviceCollection.BuildServiceProvider());
            configuration.UseTimers();
            var host = new JobHost(configuration);
            host.RunAndBlock();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            serviceCollection.AddDbContext<MessageContext>(options =>
                     options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            serviceCollection.AddScoped<WebJobsMethods, WebJobsMethods>();
            serviceCollection.AddTransient<IMessageRepositoryService, MessageRepositoryService>();
            serviceCollection.AddTransient<IMessageRepository, MessageRepository>();
            serviceCollection.AddTransient<IDataHandlerService, DataHandlerService>();
            serviceCollection.AddTransient<IDataHandler, DataHandler>();
            Environment.SetEnvironmentVariable("AzureWebJobsDashboard", configuration.GetConnectionString("AzureWebJobsDashboard"));
            Environment.SetEnvironmentVariable("AzureWebJobsStorage", configuration.GetConnectionString("AzureWebJobsStorage"));
        }
    }
}
